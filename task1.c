#include<stdlib.h>
#include<stdio.h>

typedef struct _Matrix
{
    int rows;
    int col;
	int** mat;
	struct _Matrix (*new_matrix)(int rows,int col);
	int ** (*mat_product)(struct _Matrix m1,struct _Matrix m2);

}matr;

typedef struct _vector
{
   struct _Matrix matx;
   struct _vector (*new_vector)(int rows,int col);
   int ** (*vec_product)(struct _Matrix m1,struct _vector m2);
	
}vec;


struct _Matrix new_matrix(int rows, int col){    //constructor
	struct _Matrix mat;
	int i;
	mat.rows=rows;
	mat.col=col;

	mat.mat=(int **) malloc(rows* sizeof(int *));

	for(i=0;i<rows;i++)
	{
	mat.mat[i]=(int *)malloc(col * sizeof(int *));

	}

	

	return mat;
}


struct _vector new_vector(int rows, int col){    //constructor
	vec v;
	matr m4;
	int i;
	v.matx.rows=rows;
	v.matx.col=col;

	v.matx.mat=(int **)malloc(rows* sizeof(int*));

	for(i=0;i<rows;i++){
	v.matx.mat[i]=(int *)malloc(col*sizeof(int*));

	}

	return v;
}

int **mat_product(matr m1,matr m2 ){
	int i,a,j,k;
	int ** result;
	result=(int **)malloc(m1.rows * sizeof (*result));


	for ( i = 0; i < m1.rows; i++)
    {
        /* calloc initializes all to '0' */
		result[i] = (int *)malloc (m2.col * sizeof (**result));
        // if (!result[i]) throw error
    }

	for ( i = 0; i < m1.rows; i++)
    {
		for ( j = 0; j < m2.col; j++)
        {
			for ( k = 0; k < m2.rows; k++)
            {
                result [i][j] += m1.mat[i][k] *  m2.mat[k][j];
            }
        }
    }

	return result;
}
int **vec_product(matr m1,vec m2 ){
	int i,j,k;
	int ** result;
	result=(int **)malloc(m1.rows * sizeof (*result));


	for ( i = 0; i < m1.rows; i++)
    {
        /* calloc initializes all to '0' */
		result[i] = (int *)calloc (m2.matx.col, sizeof (**result));
        // if (!result[i]) throw error
    }

	for ( i = 0; i < m1.rows; i++)
    {
		for ( j = 0; j < m2.matx.col; j++)
        {
			for ( k = 0; k < m2.matx.rows; k++)
            {
				result [i][j] += m1.mat[i][k] *  m2.matx.mat[k][j];
            }
        }
    }

	return result;
}
void l1Norm (struct _Matrix *m){
    int i, j;
	int k = 0;
	int mat_l1[25];


	for (i = 0; i < m->col; i++) {
		mat_l1[i] = 0;
		for (j = 0; j < m->rows; j++) {
			mat_l1[i] += m->mat[j][i];
		}
	}

	for (i = 0; i < m->col; i++){
		if (mat_l1[i] > k)
			k = mat_l1[i];
	}

	printf(" L1Norm %d\n", k);
    return;
}

void printm(int **m, int row, int col)
{
	int i,j;
    for(i = 0; i < row; i++)
    {
        for( j = 0; j < col; j++)
            printf("%d ", m[i][j]);
        printf("\n");
    }
}

int main(){
	int i,j;
	int ** result;
	matr m1=new_matrix(2,2);
	matr m2=new_matrix(2,2);

	for( i=0;i<2;i++){
	
		for( j=0;j<2;j++){
		
			m1.mat[i][j]=1;
			m2.mat[i][j]=2;
		}
	
	}
	result=m1.mat_product(m1,m2);


	//printm(result,2,2);
	//matr.mat_product(m1,m2);



}